 
-- | deletes first arg from second arg
-- >>> deleteInt 1 [1,2,1,3,4]
-- [2,3,4]
--
-- >>> deleteInt 1 [2,3]
-- [2,3]
--
-- >>> deleteInt 1 []
-- []
deleteInt :: Int -> [Int] -> [Int]
deleteInt n [] = []
deleteInt n (x:xs) | n == x = deleteInt n xs
                 | otherwise = (x : (deleteInt n xs)) 
             
-- | returns list of indices of first arg in second arg
-- >>> findIndices 1 [1,2,1,3,4]
-- [0,2]
--
-- >>> findIndices 1 [2,3]
-- []
findIndicesIntHelper :: Int -> Int -> [Int] -> [Int]
findIndicesIntHelper index n [] = []
findIndicesIntHelper index n (x:xs) | n == x = index : (findIndicesIntHelper (index+1) n xs)
                                      | otherwise = findIndicesIntHelper (index+1) n xs
 
findIndicesInt :: Int -> [Int] -> [Int]
findIndicesInt = findIndicesIntHelper 0
 
-- | tribo_n = tribo_{n-1} + tribo_{n-2} + tribo_{n-3}
--   tribo_0 = 1
--   tribo_1 = 1
--   tribo_2 = 1
--
-- >>> tribo 0
-- 1
--
-- >>> tribo 1
-- 1
--
-- >>> tribo 2
-- 1
--
-- >>> tribo 3
-- 3
--
-- >>> odd (tribo 100)
-- True
tribo :: Int -> Int
tribo n = tribo' n 1 1 1
  where tribo' 0 a b c = a
        tribo' 1 a b c = b
        tribo' 2 a b c = c
        tribo' n a b c = tribo' (n-1) b c $ (a+b+c)
 
--tribo :: Int -> Int
--tribo n = let
--          tribo' 1 a b c = a
--          tribo' 2 a b c = b
--          tribo' 3 a b c = c
--          tribo' n a b c = tribo' (n-1) b c $ a+b+c
--         in tribo' n 1 1 1
 
-- Тип Цвет, который может быть Белым, Черным, Красным, Зелёным, Синим, либо Смесь из трёх чисел (0-255)
-- операции сложение :: Цвет -> Цвет -> Цвет
-- (просто складываются интенсивности, если получилось >255, то ставится 255)
-- ПолучитьКрасныйКанал, ПолучитьЗелёныйКанал, ПолучитьСинийКанал :: Цвет -> Int
data Color = White
             | Black
             | Red
             | Green
             | Blue
             | RGB { r, g, b ::Int}
             deriving (Show)
color:: Color -> Color
color White = RGB {r = 255, g=255,b = 255 }
color Black = RGB {r = 0, g=0,b = 0 }
color Red = RGB {r = 255, g=0,b = 0 }
color Green = RGB {r = 0, g=255,b = 0 }
color Blue = RGB {r = 255, g=0,b = 255 }
color c = case c of
               RGB _ _ _-> c
               _ -> error "Oops!"
 
 
r_c :: Color -> Int
r_c c = r (color c)
 
g_c :: Color -> Int
g_c c = g (color c)
 
b_c :: Color -> Int
b_c c = b (color c)
 
sum_chanell :: (Color -> Int) -> (Color) -> (Color) -> Int
sum_chanell func c1 c2 =
                (func (color c1)) + (func (color c2));
 
add :: Color -> Color -> Color
add color1 color2 = let
                  r' = if res > 255 then 255 else res where res = sum_chanell r_c color1 color2
                  g' = if res > 255 then 255 else res where res = sum_chanell g_c color1 color2
                  b' = if res > 255 then 255 else res where res = sum_chanell b_c color1 color2
                 in RGB r' g' b'
