{- 
 -
 -                4
 - pi = -------------------
 -                1^2
 -       1 + --------------
 -                   3^2
 -            2 + ---------
 -                     5^2
 -                 2 + ----
 -                      ...
 -}
pi1 n = 4 / f1 n 1
f1 n t = if t==n then 2 + (2*t-1)*(2*t-1) else (if t==1 then 1 else 2) + (2*t-1)*(2*t-1) / f1 n (t+1) 

{-
 -                  1^2
 - pi = 3 + -------------------
 -                   3^2
 -           6 + -------------
 -                     5^2
 -                6 + ------
 -                     ...
 -}	
pi2 n = f2 n 1
f2 n t = if t==n then 6 + (2*t-1)*(2*t-1) else (if t==1 then 3 else 6) + (2*t-1)*(2*t-1) / f2 n (t+1) 

{-
 -                 4
 - pi = ------------------------
 -                   1^2
 -       1 + ------------------
 -                    2^2
 -            3 + -----------
 -                     3^2
 -                 5 + ---
 -                     ...
 -}
pi3 n = 4 / f3 n 1
f3 n t = if t==n then (2*t-1) + t*t else (2*t-1) + t*t / f3 n (t+1)
 
{-       4     4     4     4
 - pi = --- - --- + --- - --- + ...
 -       1     3     5     7
 -}
pi4 n = 
	if n==1 then 4 else (if even n then (-4)/(2*fromIntegral(n)-1) else 4/(2*fromIntegral(n)-1)) + pi4 (n-1)


{-             4         4         4         4
 - pi = 3 + ------- - ------- + ------- - -------- + ...
 -           2*3*4     4*5*6     6*7*8     8*9*10
 -}
pi5 n = 
	if n==1 then 3 else (if even n then 4 / fromIntegral((2*n-2) * (2*n-1) * 2*n) else -4 / fromIntegral((2*n-2) * (2*n-1) * 2*n))  + pi5 (n-1)

{-
       x^1     x^2
e^x = ----- + ----- + ...
        1!      2!
-}
e x n = 
	if n == 0 then 0 else (((x)**(n))/ factorial n) + e x (n-1)
factorial n = product [1..n]
