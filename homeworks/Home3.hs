data Expr = Val Int
          | Plus Expr Expr
          | Minus Expr Expr
          | Mult Expr Expr
          | Div Expr Expr
          | Mod Expr Expr
            deriving (Eq,Show)
 
eval :: Expr -> Int
eval (Val value)         = value
eval (Plus a b)  = eval a + eval b
eval (Minus a b) = eval a - eval b
eval (Mult a b)  = eval a * eval b
eval (Div a b)   = eval a `div` eval b
eval (Mod a b)   = eval a `mod` eval b



