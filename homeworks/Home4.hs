-- | Нахождение максимум и его индекс 
-- >>> myMaxIndex [1,5,2,3,4]
-- (1,5)
{-
- maxPair :: (Int,Integer) -> (Int,Integer) -> (Int,Integer)
- maxIndex :: [Integer] -> Int -> (Int,Integer) -> (Int,Integer)
-}
myMaxIndex :: [Integer] -> (Int,Integer)
myMaxIndex (x:xs) = maxIndex xs 1 (0,x)
	where maxIndex (x:xs) i (imax,max) = maxIndex xs (i+1) $ maxPair (imax,max) (i,x) 
	      maxIndex [] i (imax,max) = (imax,max)
	      maxPair (i,v) (j,w) = if v > w then (i,v) else (j,w)


-- | Количество элементов, равных максимальному
-- >>> maxCount [1,5,3,10,3,10,5]
-- 2
maxCount :: [Integer] -> Int
maxCount xs = myLength 0 $ findCount xs
    where findCount xs = let max = myMaximum xs
                         in compare max xs [] -- массив элементов, равных максимальному
          compare max [] [] = []
          compare max (x:xs) acc | x == max = compare max xs $ x : acc 
                                        | otherwise = compare max xs acc       
          compare max [] acc = acc
          myLength i [] = i
          myLength i (x:xs) = myLength (i+1) xs

myMaximum :: [Integer] -> Integer
myMaximum [x] = x
myMaximum (x:y:xs) =  myMaximum $ (myMax x y) : xs
		where myMax x y | x > y = x
						| otherwise = y


-- | Количество элементов между минимальным и максимальным
-- >>> countBetween [-1,3,100,3]
-- 2
--
-- >>> countBetween [100,3,-1,3]
-- -2
--
-- >>> countBetween [-1,100]
-- 1
--
-- >>> countBetween [1]
-- 0
countBetween :: [Integer] -> Int
countBetween [] = 0
countBetween [x,y] = 1
countBetween xs = count xs
		where myMinIndex (x:xs) = minIndex xs 1 (0,x)
		      minIndex (x:xs) i (imax,max) = minIndex xs (i+1) $ minPair (imax,max) (i,x) -- i - счётчик для сохранения значения индекса текущего элемента
		      minIndex [] i (imax,max) = (imax,max)
		      minPair (i,v) (j,w) = if v < w then (i,v) else (j,w)
		      count xs = let (imax,max) = myMaxIndex xs
		                     (imin,min) = myMinIndex xs
		      		     in if imin > imax then (getCount 0 max min xs)*(-1) else (getCount 0 max min xs) 
		          
		      getCount k max min (x:xs) | x < max && x > min = getCount (k+1) max min xs
		      						    | otherwise = getCount k max min xs
		      getCount k max min [] = k